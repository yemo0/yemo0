import{_ as n,d as s}from"./app.66431418.js";const a={},e=s(`<h2 id="\u53D8\u91CF" tabindex="-1">\u53D8\u91CF</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>//\u6574\u6570\u7C7B\u578B
int num = 10;    // \u5B9A\u4E49int\u7C7B\u578B\u7684num
print(num);
var myNum = 200;    // \u4F7F\u7528var\u81EA\u52A8\u5224\u65AD\u7C7B\u578B
print(myNum);

//\u5E03\u5C14\u7C7B\u578B
bool ok = true; // bool\u53EA\u6709true\u548Cfalse
print(ok);

//\u5B57\u7B26\u4E32\u7C7B\u578B
String str = &quot;hello&quot;; // hello
String str2 = &#39;hello&#39;;  // hello
String str3 = &quot;&quot;&quot; // \u4E09\u4E2A\u53CC\u5F15\u53F7\u6216\u5355\u5F15\u53F7\u53EF\u4EE5\u5199\u591A\u884C\u5B57\u7B26\u4E32sayhello&quot;&quot;&quot;;

//\u6D6E\u70B9\u6570\u7C7B\u578B
double b = 9.99;
print(b);
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br></div></div><h2 id="\u6570\u7EC4\u7C7B\u578B" tabindex="-1">\u6570\u7EC4\u7C7B\u578B</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>List arr = [10, 100, 50];
print(arr[0]); // \u6253\u5370\u7B2C\u4E00\u4E2A
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><h2 id="\u5B57\u5178\u7C7B\u578B" tabindex="-1">\u5B57\u5178\u7C7B\u578B</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>// one
var m = { // key\u5FC5\u987B\u8981\u52A0\u5F15\u53F7
&quot;name&quot;: &quot;panghu&quot;,
&quot;age&quot;: 100,
&quot;isOK&quot;: true,
&quot;work&quot;: [&quot;study&quot;, &quot;code&quot;]
};

print(m); // {name: panghu, age: 100, isOK: true, work: [study, code]}
print(m[&quot;name&quot;]); // panghu

// two
Map m2 = {
&quot;name&quot;: &quot;panghu&quot;,
&quot;age&quot;: 100
};

// two
var m3 = new Map();
m3[&quot;name&quot;] = &quot;admin&quot;;
m3[&quot;age&quot;] = 1;
print(m3);
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br></div></div><h2 id="\u5224\u65AD\u7C7B\u578B" tabindex="-1">\u5224\u65AD\u7C7B\u578B</h2><p>\u4F7F\u7528is\u5173\u952E\u5B57\u5224\u65AD\u6570\u636E\u7C7B\u578B</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var n = 1;
if (n is String) {
print(&quot;String&quot;);
} else if(n is int) {
print(&quot;int&quot;);
} else {
print(&quot;Other types&quot;);
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br></div></div><h2 id="\u8FD0\u7B97\u7B26" tabindex="-1">\u8FD0\u7B97\u7B26</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var a = 2;
var b = 2;

// \u6570\u5B66\u8FD0\u7B97\u7B26
print(a * b); // \u4E58
print(a + b); // \u52A0
print(a - b); // \u51CF
print(a / b); // \u9664
print(a % b); // \u53D6\u4F59
print(a ~/ b); // \u53D6\u6574

// \u903B\u8F91\u8FD0\u7B97\u7B26
print(a == b);  // \u7B49\u4E8E
print(a != b);  // \u4E0D\u7B49\u4E8E
print(a &gt; b); // \u5927\u4E8E
print(a &gt;= b); // \u5927\u4E8E\u7B49\u4E8E
print(a &lt; b); // \u5C0F\u4E8E\u7B49\u4E8E
print(a &lt;= b); // \u5927\u4E8E\u7B49\u4E8E

print(true &amp;&amp; false); // \u6709\u4E00\u4E2A\u4E0D\u4E3Atrue\u8FD4\u56DEfalse
print(true || false); // \u6709\u4E00\u4E2A\u4E0D\u4E3Afalse\u8FD4\u56DEtrue

var c = 1;
c ??= 100;  // \u5982\u679Cc\u7B49\u4E8E\u7A7A\u5C31\u5427100\u8D4B\u503C\u7ED9\u5B83

// \u7B26\u5408\u8FD0\u7B97\u7B26 += -= *= /= %= ~/=
print(a += 100);

// \u4E09\u5143\u8FD0\u7B97\u7B26
var n = 1;
n == 1 ? print(&quot;a\u4E0D\u7B49\u4E8E1&quot;) : print(&quot;a\u7B49\u4E8E1&quot;);
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br></div></div><h2 id="\u903B\u8F91\u8FD0\u7B97\u7B26" tabindex="-1">\u903B\u8F91\u8FD0\u7B97\u7B26</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var t = true;
if (t) {
print(true);
} else {
print(false);
}

var n = 10;
switch(n) {
case 1:
  print(&quot;n == 1&quot;);
  break;  // break\u6EE1\u8DB3\u6761\u4EF6\u8DF3\u51FA\u51FD\u6570,\u9632\u6B62case\u7A7F\u900F
case 5:
  print(&quot;n == 5&quot;);
  break;
case 10:
  print(&quot;n == 10&quot;);
  break;
default:
  print(&quot;\u672A\u627E\u5230\u7B26\u5408\u6761\u4EF6&quot;);
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br></div></div><h2 id="\u4E09\u5143\u8FD0\u7B97\u7B26" tabindex="-1">\u4E09\u5143\u8FD0\u7B97\u7B26</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var n = 10;
n &gt; 5 ? print(&quot;a\u5927\u4E8E5&quot;) : print(&quot;a \u4E0D\u5927\u4E8E5&quot;); // a\u5927\u4E8E5
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><h2 id="\u662F\u5426\u4E3A\u7A7A" tabindex="-1">\u662F\u5426\u4E3A\u7A7A</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var n;
var c = n ?? 20;  // \u5982\u679Cn\u4E3A\u7A7A\u8D4B\u503C20,\u5426\u5219\u8D4B\u503Cn\u7684\u503C\u7ED9c
print(c);
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><h2 id="\u7C7B\u578B\u8F6C\u6362" tabindex="-1">\u7C7B\u578B\u8F6C\u6362</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>// \u7C7B\u578B\u8F6C\u6362
var str = &quot;100&quot;;
var num = int.parse(str); // \u5C06String\u8F6C\u6362\u4E3Aint\u7C7B\u578B
print(num is int);  // true

var str2 = &quot;9.99&quot;;
var d = double.parse(str2); // \u5C06String\u8F6C\u6362\u4E3Adouble
print(d is double);   // true

var str3 = &quot;&quot;;
try {
var t = double.parse(str3); // \u5B57\u7B26\u4E32\u4E3A\u7A7A\u4F1A\u62A5\u9519
} catch(err) {
str3 = &quot;0&quot;;
}

var n = 10;
var str4 = n.toString();  // \u5C06\u6574\u6570\u8F6C\u6362\u4E3A\u5B57\u7B26\u4E32
print(str4 is String);  // true
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br></div></div><h2 id="\u5224\u65AD\u5B57\u7B26\u4E32\u662F\u5426\u4E3A\u7A7A" tabindex="-1">\u5224\u65AD\u5B57\u7B26\u4E32\u662F\u5426\u4E3A\u7A7A</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var a = &quot;&quot;;
if (a.isEmpty) {
print(&quot;\u4E3A\u7A7A&quot;);
} else {
print(&quot;\u4E0D\u4E3A\u7A7A&quot;);
}

var b = &quot;&quot;;
b.isNotEmpty ? print(&quot;\u4E3A\u7A7A&quot;) : print(&quot;\u4E0D\u4E3A\u7A7A&quot;);  // \u4E0D\u4E3A\u7A7A
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br></div></div><h2 id="\u5FAA\u73AF" tabindex="-1">\u5FAA\u73AF</h2><p>break \u8DF3\u51FA\u5F53\u524D\u5FAA\u73AF(\u53EA\u80FD\u8DF3\u51FA\u4E00\u5C42)\u3002<br> continue \u8DF3\u51FA\u5F53\u524D\u5FAA\u73AF,\u7EE7\u7EED\u4E0B\u6B21\u5FAA\u73AF</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var sum = 0;
for (var i = 1; i &lt;= 100; i++) {
  sum+=i;
}
print(sum);

// two
var sum2 = 0;
var i2 = 1;
while(i2 &lt;= 100) {
sum2+= i2;
i2++;
}
print(sum2);

// Three
var sum3 = 0;
var i3 = 1;
do{
sum3+=i3;
i3++;
} while(i3 &lt;= 100);
print(sum3);
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br></div></div>`,24);function r(l,p){return e}var i=n(a,[["render",r],["__file","grammar.html.vue"]]);export{i as default};
