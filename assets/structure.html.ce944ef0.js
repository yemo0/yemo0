import{_ as n,d as s}from"./app.66431418.js";const a={},p=s(`<h2 id="\u5B9A\u4E49\u7ED3\u6784\u4F53" tabindex="-1">\u5B9A\u4E49\u7ED3\u6784\u4F53</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">type</span> Student <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	id	<span class="token builtin">int</span>
	name <span class="token builtin">string</span>
	sex	<span class="token builtin">byte</span>
	age <span class="token builtin">int</span>
	address	<span class="token builtin">string</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br></div></div><h3 id="\u5B9E\u4F8B" tabindex="-1">\u5B9E\u4F8B</h3><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token comment">// \u5B9A\u4E49\u4E00\u4E2A\u7ED3\u6784\u4F53</span>
<span class="token keyword">type</span> Student <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	id	<span class="token builtin">int</span>
	name <span class="token builtin">string</span>
	sex	<span class="token builtin">byte</span>
	age <span class="token builtin">int</span>
	address	<span class="token builtin">string</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	<span class="token comment">// \u987A\u5E8F\u521D\u59CB\u5316,\u6BCF\u4E2A\u6210\u5458\u5FC5\u987B\u521D\u59CB\u5316</span>
	<span class="token keyword">var</span> s1 Student <span class="token operator">=</span> Student<span class="token punctuation">{</span><span class="token number">1</span><span class="token punctuation">,</span> <span class="token string">&quot;panghu&quot;</span><span class="token punctuation">,</span> <span class="token char">&#39;m&#39;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">,</span> <span class="token string">&quot;china&quot;</span><span class="token punctuation">}</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>s1<span class="token punctuation">)</span>	<span class="token comment">// \u8F93\u51FA{1 panghu 109 18 china}</span>

	<span class="token comment">// \u6307\u5B9A\u6210\u5458\u521D\u59CB\u5316,\u6CA1\u6709\u521D\u59CB\u5316\u7684\u6210\u5458,\u81EA\u52A8\u8D4B\u503C\u4E3A0</span>
	s2 <span class="token operator">:=</span> Student<span class="token punctuation">{</span>name<span class="token punctuation">:</span> <span class="token string">&quot;panghu2&quot;</span><span class="token punctuation">,</span> id<span class="token punctuation">:</span> <span class="token number">2</span><span class="token punctuation">}</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>s2<span class="token punctuation">)</span>	<span class="token comment">// \u8F93\u51FA{2 panghu2 0 0 } </span>

	<span class="token comment">//\u7ED3\u6784\u4F53\u6307\u9488\u53D8\u91CF\u521D\u59CB\u5316</span>
	<span class="token keyword">var</span> p1 <span class="token operator">*</span>Student <span class="token operator">=</span> <span class="token operator">&amp;</span>Student<span class="token punctuation">{</span><span class="token number">1</span><span class="token punctuation">,</span> <span class="token string">&quot;panghu&quot;</span><span class="token punctuation">,</span> <span class="token char">&#39;m&#39;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">,</span> <span class="token string">&quot;china&quot;</span><span class="token punctuation">}</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token operator">*</span>p1<span class="token punctuation">)</span> <span class="token comment">// \u8F93\u51FA{1 panghu 109 18 china}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br></div></div><h2 id="\u7ED3\u6784\u4F53\u666E\u901A\u53D8\u91CF" tabindex="-1">\u7ED3\u6784\u4F53\u666E\u901A\u53D8\u91CF</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token comment">// \u5B9A\u4E49\u4E00\u4E2A\u7ED3\u6784\u4F53</span>
<span class="token keyword">type</span> Student <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	id	<span class="token builtin">int</span>
	name <span class="token builtin">string</span>
	sex	<span class="token builtin">byte</span>
	age <span class="token builtin">int</span>
	address	<span class="token builtin">string</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	<span class="token comment">// \u5B9A\u4E49\u4E00\u4E2A\u666E\u901A\u7ED3\u6784\u4F53\u53D8\u91CF</span>
	<span class="token keyword">var</span> s Student
	<span class="token comment">// \u64CD\u4F5C\u6210\u5458,\u9700\u8981\u4F7F\u7528(.)\u8FD0\u7B97\u7B26</span>
	s<span class="token punctuation">.</span>id <span class="token operator">=</span> <span class="token number">1</span>
	s<span class="token punctuation">.</span>name <span class="token operator">=</span> <span class="token string">&quot;panghu&quot;</span>
	s<span class="token punctuation">.</span>sex <span class="token operator">=</span> <span class="token char">&#39;m&#39;</span>
	s<span class="token punctuation">.</span>age <span class="token operator">=</span> <span class="token number">18</span>
	s<span class="token punctuation">.</span>address <span class="token operator">=</span> <span class="token string">&quot;china&quot;</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>s<span class="token punctuation">)</span>	<span class="token comment">// \u8F93\u51FA{1 panghu 109 18 china}</span>
	<span class="token comment">// \u901A\u8FC7new\u7533\u8BF7\u4E00\u4E2A\u7ED3\u6784\u4F53</span>
	p2 <span class="token operator">:=</span> <span class="token function">new</span><span class="token punctuation">(</span>Student<span class="token punctuation">)</span>
	p2<span class="token punctuation">.</span>id <span class="token operator">=</span> <span class="token number">1</span>
	p2<span class="token punctuation">.</span>name <span class="token operator">=</span> <span class="token string">&quot;miku&quot;</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token operator">*</span>p2<span class="token punctuation">)</span> <span class="token comment">// \u8F93\u51FA{1 miku 0 0 }</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br></div></div><h3 id="\u7ED3\u6784\u4F53\u505A\u51FD\u6570\u53C2\u6570-\u5740\u4F20\u9012" tabindex="-1">\u7ED3\u6784\u4F53\u505A\u51FD\u6570\u53C2\u6570(\u5740\u4F20\u9012)</h3><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token comment">// \u5B9A\u4E49\u4E00\u4E2A\u7ED3\u6784\u4F53</span>
<span class="token keyword">type</span> Student <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	id	<span class="token builtin">int</span>
	name <span class="token builtin">string</span>
	sex	<span class="token builtin">byte</span>
	age <span class="token builtin">int</span>
	address	<span class="token builtin">string</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token function">test01</span><span class="token punctuation">(</span>s <span class="token operator">*</span>Student<span class="token punctuation">)</span> <span class="token punctuation">{</span>
	s<span class="token punctuation">.</span>id <span class="token operator">=</span> <span class="token number">111</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>s<span class="token punctuation">)</span>	<span class="token comment">// {111 panghu 109 18 china}</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	s <span class="token operator">:=</span> Student<span class="token punctuation">{</span><span class="token number">1</span><span class="token punctuation">,</span> <span class="token string">&quot;panghu&quot;</span><span class="token punctuation">,</span> <span class="token char">&#39;m&#39;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">,</span> <span class="token string">&quot;china&quot;</span><span class="token punctuation">}</span>
	<span class="token function">test01</span><span class="token punctuation">(</span><span class="token operator">&amp;</span>s<span class="token punctuation">)</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>s<span class="token punctuation">)</span>	<span class="token comment">// {111 panghu 109 18 china}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br></div></div><h2 id="\u533F\u540D\u5B57\u6BB5" tabindex="-1">\u533F\u540D\u5B57\u6BB5</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">type</span> Person <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	name <span class="token builtin">string</span>
	sex <span class="token builtin">byte</span>
	age <span class="token builtin">int</span>
<span class="token punctuation">}</span>
<span class="token keyword">type</span> Student <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	Person <span class="token comment">// \u53EA\u6709\u7C7B\u578B,\u6CA1\u6709\u540D\u5B57,\u533F\u540D\u5B57\u6BB5,\u7EE7\u627FPerson\u7684\u6210\u5458</span>
	id <span class="token builtin">int</span>
	address <span class="token builtin">string</span>
<span class="token punctuation">}</span>
<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	<span class="token comment">// \u987A\u5E8F\u521D\u59CB\u5316</span>
	<span class="token keyword">var</span> s1 Student <span class="token operator">=</span> Student<span class="token punctuation">{</span>Person<span class="token punctuation">{</span><span class="token string">&quot;panghu&quot;</span><span class="token punctuation">,</span> <span class="token char">&#39;m&#39;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">}</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token punctuation">,</span> <span class="token string">&quot;china&quot;</span><span class="token punctuation">}</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>s1<span class="token punctuation">)</span>	<span class="token comment">// {{panghu 109 18} 1 china}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br></div></div><h2 id="\u975E\u7ED3\u6784\u4F53\u533F\u540D\u5B57\u6BB5" tabindex="-1">\u975E\u7ED3\u6784\u4F53\u533F\u540D\u5B57\u6BB5</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">type</span> myAddress <span class="token builtin">string</span> <span class="token comment">// \u81EA\u5B9A\u4E49\u7C7B\u578B</span>
<span class="token keyword">type</span> Person <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	name <span class="token builtin">string</span>
	sex <span class="token builtin">byte</span>
	age <span class="token builtin">int</span>
<span class="token punctuation">}</span>
<span class="token keyword">type</span> Student <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	Person <span class="token comment">// \u53EA\u6709\u7C7B\u578B,\u6CA1\u6709\u540D\u5B57,\u533F\u540D\u5B57\u6BB5,\u7EE7\u627FPerson\u7684\u6210\u5458</span>
	<span class="token builtin">int</span>
	myAddress
<span class="token punctuation">}</span>
<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	s  <span class="token operator">:=</span> Student<span class="token punctuation">{</span>Person<span class="token punctuation">{</span><span class="token string">&quot;panghu&quot;</span><span class="token punctuation">,</span> <span class="token char">&#39;m&#39;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">}</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token punctuation">,</span> <span class="token string">&quot;china&quot;</span><span class="token punctuation">}</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>s<span class="token punctuation">)</span>	<span class="token comment">// {{panghu 109 18} 1 china}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br></div></div><h2 id="\u65B9\u6CD5" tabindex="-1">\u65B9\u6CD5</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">type</span> Person <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	name <span class="token builtin">string</span>
	sex <span class="token builtin">byte</span>
	age <span class="token builtin">int</span>
<span class="token punctuation">}</span>
<span class="token comment">// \u5E26\u6709\u63A5\u6536\u8005\u7684\u51FD\u6570\u53EB\u65B9\u6CD5</span>
<span class="token keyword">func</span> <span class="token punctuation">(</span>tmp Person<span class="token punctuation">)</span> <span class="token function">PrintInfo</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>tmp<span class="token punctuation">)</span>	<span class="token comment">// \u8F93\u51FA{panghu 109 18}</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	p <span class="token operator">:=</span> Person<span class="token punctuation">{</span><span class="token string">&quot;panghu&quot;</span><span class="token punctuation">,</span> <span class="token char">&#39;m&#39;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">}</span>
	p<span class="token punctuation">.</span><span class="token function">PrintInfo</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br></div></div><h2 id="\u65B9\u6CD5\u7684\u7EE7\u627F" tabindex="-1">\u65B9\u6CD5\u7684\u7EE7\u627F</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">type</span> Person <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	name <span class="token builtin">string</span>
	age <span class="token builtin">int</span>
<span class="token punctuation">}</span>

<span class="token keyword">type</span> Student <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	Person	<span class="token comment">// \u533F\u540D\u5B57\u6BB5</span>
	address <span class="token builtin">string</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	v <span class="token operator">:=</span> Student<span class="token punctuation">{</span>Person<span class="token punctuation">{</span><span class="token string">&quot;panghu&quot;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">}</span><span class="token punctuation">,</span> <span class="token string">&quot;china&quot;</span><span class="token punctuation">}</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>v<span class="token punctuation">)</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br></div></div><h2 id="\u65B9\u6CD5\u503C" tabindex="-1">\u65B9\u6CD5\u503C</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code>
<span class="token keyword">type</span> Person <span class="token keyword">struct</span> <span class="token punctuation">{</span>
	name <span class="token builtin">string</span>
	age <span class="token builtin">int</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token punctuation">(</span>p Person<span class="token punctuation">)</span> <span class="token function">GetInfo</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;i am GetInfo&quot;</span><span class="token punctuation">)</span>
<span class="token punctuation">}</span>

<span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	p <span class="token operator">:=</span> Person<span class="token punctuation">{</span><span class="token string">&quot;name&quot;</span><span class="token punctuation">,</span> <span class="token number">18</span><span class="token punctuation">}</span>
	v <span class="token operator">:=</span> p<span class="token punctuation">.</span>GetInfo
	<span class="token function">v</span><span class="token punctuation">(</span><span class="token punctuation">)</span>	<span class="token comment">// \u7B49\u4EF7\u4E8Ep.GetInfo()</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br></div></div>`,18);function t(e,c){return p}var l=n(a,[["render",t],["__file","structure.html.vue"]]);export{l as default};
