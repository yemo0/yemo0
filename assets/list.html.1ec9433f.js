import{_ as n,d as s}from"./app.66431418.js";const a={},e=s(`<h2 id="list\u7684\u64CD\u4F5C" tabindex="-1">List\u7684\u64CD\u4F5C</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>// list\u5C5E\u6027
List l = [&quot;\u82F9\u679C&quot;, &quot;\u897F\u74DC&quot;, &quot;\u54C8\u5BC6\u74DC&quot;];
print(l.length);  // \u8F93\u51FA\u957F\u5EA6 3
print(l.isEmpty);  // \u662F\u5426\u4E3A\u7A7A  false
print(l.isNotEmpty); // \u662F\u5426\u4E0D\u4E3A\u7A7A true
print(l.reversed); // \u6570\u7EC4\u53CD\u8F6C  (\u54C8\u5BC6\u74DC, \u897F\u74DC, \u82F9\u679C)

var myList = l.reversed.toList(); // \u96C6\u5408
print(myList);  // [\u54C8\u5BC6\u74DC, \u897F\u74DC, \u82F9\u679C]

// List\u65B9\u6CD5
myList.add(&quot;\u7315\u7334\u6843&quot;);  // \u589E\u52A0\u4E00\u4E2A\u6570\u636E
myList.addAll([&quot;\u6843\u5B50&quot;, &quot;\u8461\u8404&quot;]);  //\u62FC\u63A5\u6570\u636E
print(myList);  // [\u54C8\u5BC6\u74DC, \u897F\u74DC, \u82F9\u679C, \u7315\u7334\u6843, \u6843\u5B50, \u8461\u8404]

print(myList.indexOf(&quot;\u897F\u74DC&quot;));  // \u67E5\u627E\u6570\u636E \u627E\u4E0D\u5230\u8FD4\u56DE-1
myList.remove(&quot;\u82F9\u679C&quot;); // \u79FB\u9664\u6570\u636E
print(myList);  // [\u54C8\u5BC6\u74DC, \u897F\u74DC, \u7315\u7334\u6843, \u6843\u5B50, \u8461\u8404]

myList.fillRange(1, 2, &quot;\u897F\u74DC1&quot;); // \u4FEE\u6539
print(myList);  // [\u897F\u74DC1, \u897F\u74DC, \u7315\u7334\u6843, \u6843\u5B50, \u8461\u8404]

myList.insert(1, &quot;\u8354\u679D&quot;); // \u63D2\u5165\u6570\u636E
print(myList);  // [\u54C8\u5BC6\u74DC, \u8354\u679D, \u897F\u74DC1, \u7315\u7334\u6843, \u6843\u5B50, \u8461\u8404]

myList.insertAll(3, [&quot;\u897F\u74DC2&quot;, &quot;\u897F\u74DC3&quot;]);  // \u63D2\u5165\u591A\u4E2A\u6570\u636E
print(myList);

print(myList.join(&quot;-&quot;));   // &quot;\u8F6C\u6362\u4E3A\u5B57\u7B26\u4E32\u7528-\u5206\u5272(\u4E5F\u53EF\u4EE5\u4F7F\u7528\u522B\u5230)&quot;

var str = &quot;\u897F\u74DC-\u54C8\u5BC6\u74DC-\u6A58\u5B50&quot;;
print(str.split(&quot;-&quot;));  // \u5C06\u5B57\u7B26\u4E32\u8F6C\u6362\u4E3A\u6570\u7EC4
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br></div></div><h2 id="set" tabindex="-1">Set</h2><p>Set\u6CA1\u6709\u987A\u5E8F\u4E14\u4E0D\u80FD\u91CD\u590D\u7684\u96C6\u5408,\u65E0\u6CD5\u63D0\u4F9B\u7D22\u5F15\u83B7\u53D6\u503C</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>var s = new Set();
s.add(&quot;\u82F9\u679C&quot;);
s.add(&quot;\u9999\u8549&quot;);
s.add(&quot;\u9999\u8549&quot;);
print(s); // {\u82F9\u679C, \u9999\u8549}
print(s.toList()); // \u8F6C\u6362\u4E3Alist\u6570\u7EC4 [\u82F9\u679C, \u9999\u8549]
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br></div></div><h2 id="set\u53BB\u91CD" tabindex="-1">Set\u53BB\u91CD</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>List l = [&quot;\u82F9\u679C&quot;, &quot;\u897F\u74DC&quot;, &quot;\u54C8\u5BC6\u74DC&quot;, &quot;\u54C8\u5BC6\u74DC&quot;, &quot;\u897F\u74DC&quot;];
var s = new Set();
s.addAll(l);
print(s); // {\u82F9\u679C, \u897F\u74DC, \u54C8\u5BC6\u74DC}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br></div></div><h2 id="map\u7684\u5B9A\u4E49" tabindex="-1">Map\u7684\u5B9A\u4E49</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>Map m1 = {
  &quot;name&quot;: &quot;panghu&quot;,
  &quot;age&quot;: 10
};

var m2 = {
  &quot;name&quot;: &quot;panghu&quot;,
  &quot;age&quot;: 10
};

var m3 = new Map();
m3[&quot;name&quot;] = &quot;panghu&quot;;
m3[&quot;age&quot;] = 10;

print(&quot;$m1 $m2 $m3&quot;);
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br></div></div><h2 id="map\u7684\u57FA\u672C\u64CD\u4F5C" tabindex="-1">Map\u7684\u57FA\u672C\u64CD\u4F5C</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>Map m = {
  &quot;name&quot;: &quot;panghu&quot;,
  &quot;age&quot;: 10
};

print(m.keys);  //  (name, age)
print(m.keys.toList()); // [name, age]
print(m.isEmpty);  // \u5224\u65AD\u662F\u5426\u4E3A\u7A7A
print(m.isNotEmpty);  // \u5224\u65AD\u662F\u5426\u4E0D\u4E3A\u7A7A

m.addAll({&quot;work&quot;: &quot;code&quot;}); // \u6DFB\u52A0\u6570\u636E
print(m); //  {name: panghu, age: 10, work: code}

m.remove(&quot;work&quot;); // \u79FB\u9664
print(m); // {name: panghu, age: 10}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br></div></div><h2 id="\u5176\u4ED6\u65B9\u6CD5" tabindex="-1">\u5176\u4ED6\u65B9\u6CD5</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>List l = [&quot;\u82F9\u679C&quot;, &quot;\u897F\u74DC&quot;, &quot;\u54C8\u5BC6\u74DC&quot;];
Map m = {
&quot;name&quot;: &quot;panghu&quot;,
&quot;age&quot;: 10,
};

// one
l.forEach((v) =&gt; print(&quot;$v&quot;));

// two
l.forEach((v){
print(v);
});

// Three
m.forEach((k, v){
print(&quot;$k --- $v&quot;);
});

// \u6240\u4EE5map\u8BA9\u4ED6\u4EEC\u7684\u6570\u4E58\u4EE52
List n = [1, 2, 3];
var o = n.map((v){
return v * 2;
});
print(o); // (2, 4, 6)


List num = [1, 2, 3, 4, 5, 6, 7];
var d = num.where((v){
return v &gt; 5;
});
print(d); // (6, 7)

var t = num.any((v){  // \u8FD4\u56DEtrue\u6216false
return v &gt; 5;
});
print(t); // true
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br></div></div>`,13);function r(l,p){return e}var u=n(a,[["render",r],["__file","list.html.vue"]]);export{u as default};
