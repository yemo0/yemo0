import{_ as n,d as s}from"./app.66431418.js";const a={},p=s(`<h2 id="if" tabindex="-1">if</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">if</span> \u5E03\u5C14\u8868\u8FBE\u5F0F <span class="token punctuation">{</span>
	<span class="token comment">// \u6761\u4EF6\u8FBE\u6210\u4E3Atrue\u65F6\u6267\u884C</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><p><code> \u6848\u5217: \u8F93\u51FA\u6761\u4EF6\u4E3Atrue</code></p><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	a <span class="token operator">:=</span> <span class="token number">1</span>
	<span class="token keyword">if</span> a <span class="token operator">==</span> <span class="token number">1</span> <span class="token punctuation">{</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;\u6761\u4EF6\u4E3Atrue&quot;</span><span class="token punctuation">)</span>
	<span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br></div></div><h2 id="if-else" tabindex="-1">if else</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">if</span> \u5E03\u5C14\u8868\u8FBE\u5F0F <span class="token punctuation">{</span>
	<span class="token comment">// \u6761\u4EF6\u8FBE\u6210\u4E3Atrue\u65F6\u6267\u884C</span>
<span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
	<span class="token comment">// \u6761\u4EF6\u672A\u8FBE\u4E3Afalse\u65F6\u6267\u884C</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br></div></div><p><code> \u6848\u4F8B: \u8F93\u51FA\u6761\u4EF6\u4E3Afalse</code></p><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	a <span class="token operator">:=</span> <span class="token number">1</span>
	<span class="token keyword">if</span> a <span class="token operator">==</span> <span class="token number">2</span> <span class="token punctuation">{</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;\u6761\u4EF6\u4E3Atrue&quot;</span><span class="token punctuation">)</span>
	<span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;\u6761\u4EF6\u4E3Afalse&quot;</span><span class="token punctuation">)</span>
	<span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br></div></div><h2 id="if-else-if" tabindex="-1">if else if</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">if</span> \u5E03\u5C14\u8868\u8FBE\u5F0F <span class="token punctuation">{</span>

<span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token keyword">if</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>

<span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>

<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br></div></div><p><code>\u6848\u4F8B: \u8F93\u51FAelse if</code></p><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	a <span class="token operator">:=</span> <span class="token number">1</span>
	<span class="token keyword">if</span> a <span class="token operator">==</span> <span class="token number">2</span> <span class="token punctuation">{</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;if&quot;</span><span class="token punctuation">)</span>
	<span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token keyword">if</span><span class="token punctuation">(</span> a <span class="token operator">==</span> <span class="token number">1</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;else if&quot;</span><span class="token punctuation">)</span>
	<span class="token punctuation">}</span> <span class="token keyword">else</span> <span class="token punctuation">{</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;else&quot;</span><span class="token punctuation">)</span>
	<span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br></div></div><h2 id="switch" tabindex="-1">switch</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">switch</span> var1 <span class="token punctuation">{</span>
	<span class="token keyword">case</span> var2<span class="token punctuation">:</span>
		<span class="token operator">...</span>
	<span class="token keyword">case</span> var2<span class="token punctuation">:</span>
		<span class="token operator">...</span>	
	<span class="token keyword">default</span><span class="token punctuation">:</span>
        <span class="token operator">...</span>

<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br></div></div><p><code> \u6848\u4F8B: \u8F93\u51FA1</code></p><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	a <span class="token operator">:=</span> <span class="token number">1</span>
	<span class="token keyword">switch</span> a <span class="token punctuation">{</span>
	<span class="token keyword">case</span> <span class="token number">1</span><span class="token punctuation">:</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;1&quot;</span><span class="token punctuation">)</span>
	<span class="token keyword">case</span> <span class="token number">2</span><span class="token punctuation">:</span>
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;2&quot;</span><span class="token punctuation">)</span>
	<span class="token keyword">default</span><span class="token punctuation">:</span> 
		fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span><span class="token string">&quot;3&quot;</span><span class="token punctuation">)</span>
	<span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br></div></div><h2 id="select" tabindex="-1">select</h2><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token comment">//select\u57FA\u672C\u7528\u6CD5</span>
<span class="token keyword">select</span> <span class="token punctuation">{</span>
<span class="token keyword">case</span> <span class="token operator">&lt;-</span> chan1<span class="token punctuation">:</span>
<span class="token comment">// \u5982\u679Cchan1\u6210\u529F\u8BFB\u5230\u6570\u636E\uFF0C\u5219\u8FDB\u884C\u8BE5case\u5904\u7406\u8BED\u53E5</span>
<span class="token keyword">case</span> chan2 <span class="token operator">&lt;-</span> <span class="token number">1</span><span class="token punctuation">:</span>
<span class="token comment">// \u5982\u679C\u6210\u529F\u5411chan2\u5199\u5165\u6570\u636E\uFF0C\u5219\u8FDB\u884C\u8BE5case\u5904\u7406\u8BED\u53E5</span>
<span class="token keyword">default</span><span class="token punctuation">:</span>
<span class="token comment">// \u5982\u679C\u4E0A\u9762\u90FD\u6CA1\u6709\u6210\u529F\uFF0C\u5219\u8FDB\u5165default\u5904\u7406\u6D41\u7A0B</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br></div></div>`,18);function e(t,c){return p}var l=n(a,[["render",e],["__file","decision.html.vue"]]);export{l as default};
