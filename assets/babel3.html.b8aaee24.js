import{_ as n,d as s}from"./app.66431418.js";const a={},p=s(`<ol><li><p>\u6587\u4EF6\u76EE\u5F55</p><ul><li>index.js</li><li>xxx.js</li></ul></li><li><p>\u6587\u4EF6\u5185\u5BB9</p><ul><li><p>index.js</p><div class="language-javascript ext-js line-numbers-mode"><pre class="language-javascript"><code><span class="token keyword">import</span> x<span class="token punctuation">,</span> <span class="token punctuation">{</span> s1<span class="token punctuation">,</span> s2 <span class="token keyword">as</span> ss2 <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">&#39;./xxx.js&#39;</span>

console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>x<span class="token punctuation">)</span>
console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>s1<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token function">ss2</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br></div></div></li><li><p>xxx.js</p><div class="language-javascript ext-js line-numbers-mode"><pre class="language-javascript"><code><span class="token keyword">let</span> a <span class="token operator">=</span> <span class="token number">10</span>
<span class="token keyword">let</span> b <span class="token operator">=</span> <span class="token number">20</span>
<span class="token keyword">let</span> c <span class="token operator">=</span> <span class="token number">30</span>

<span class="token keyword">function</span> <span class="token function">show</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>a<span class="token punctuation">,</span> b<span class="token punctuation">,</span> c<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token keyword">export</span> <span class="token keyword">default</span> <span class="token punctuation">{</span>
    a<span class="token punctuation">,</span>
    c<span class="token punctuation">,</span>
    show
<span class="token punctuation">}</span>

<span class="token comment">// \u6309\u9700\u5BFC\u51FA</span>
<span class="token comment">// \u6309\u9700\u5BFC\u51FA\u53EF\u4EE5\u4F7F\u7528\u591A\u4E2A\u3001\u9ED8\u8BA4\u5BFC\u51FA\u53EA\u80FD\u51FA\u73B0\u4E00\u4E2A</span>
<span class="token keyword">export</span> <span class="token keyword">let</span> s1 <span class="token operator">=</span> <span class="token string">&#39;aaa&#39;</span>
<span class="token keyword">export</span> <span class="token keyword">function</span> <span class="token function">s2</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;s2: &quot;</span> <span class="token operator">+</span> s1<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br></div></div></li></ul></li><li><p>\u6267\u884C</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>&gt; npx babel-node .\\index.js
{ a: 10, c: 30, show: [Function: show] }
aaa
s2: aaa
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br></div></div></li></ol><h2 id="\u6267\u884C\u6A21\u5757\u4E2D\u7684\u4EE3\u7801" tabindex="-1">\u6267\u884C\u6A21\u5757\u4E2D\u7684\u4EE3\u7801</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>// \u76F4\u63A5\u5BFC\u5165\u5E76\u6267\u884C\u6A21\u5757\u4EE3\u7801
import &#39;xxx.js&#39;
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div>`,3);function e(t,l){return p}var o=n(a,[["render",e],["__file","babel3.html.vue"]]);export{o as default};
