import{_ as n,d as s}from"./app.66431418.js";const a={},e=s(`<h2 id="\u51FD\u6570\u5B9A\u4E49" tabindex="-1">\u51FD\u6570\u5B9A\u4E49</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>// \u6307\u5B9A\u8FD4\u56DE\u503C\u7C7B\u578B
int getNum() {    // \u4E5F\u53EF\u4EE5\u8FD4\u56DE\u522B\u7684\u7C7B\u578B
var num = 123;
return num;
}

var n = getNum();
print(n);
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br></div></div><h2 id="\u51FD\u6570\u7684\u53EF\u9009\u53C2\u6570" tabindex="-1">\u51FD\u6570\u7684\u53EF\u9009\u53C2\u6570</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>// \u8FD4\u56DEString\u7C7B\u578B, []\u91CC\u9762\u4EE3\u8868\u53EF\u9009\u53C2\u6570
String printUser(String name, int age, [String address, String work]) {
  return &quot;$name --- $age -- $address&quot;;
}

void main() {
  print(printUser(&quot;panghu&quot;, 10, &quot;china&quot;));
}

// \u53EF\u9009\u53C2\u6570\u53EF\u4EE5\u7528 = \u8BBE\u7F6E\u9ED8\u8BA4\u503C
String printUser(String name, int age, [String address = &quot;\u672A\u77E5&quot;]) {
  return &quot;$name --- $age -- $address&quot;;
}

void main() {
  print(printUser(&quot;panghu&quot;, 10)); // panghu --- 10 -- \u672A\u77E5
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br></div></div><h2 id="\u533F\u540D\u65B9\u6CD5-\u95ED\u5305" tabindex="-1">\u533F\u540D\u65B9\u6CD5/\u95ED\u5305</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>() {
print(&quot;say hello&quot;);
}();

// \u95ED\u5305
fun() {
var a = 123;
return () { // \u8FD4\u56DE\u533F\u540D\u51FD\u6570
  a++;
  print(a);
};
}

var f = fun();
f();  // 124
f();  // 125
f();  // 126
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br></div></div><h2 id="\u7C7B" tabindex="-1">\u7C7B</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>// \u7C7B
class Person {
  String name;
  int age;
  // void\u4EE3\u8868\u6CA1\u8FD4\u56DE\u503C
  void fun() {
    print(&quot;\${this.age}---\${this.name}&quot;);
  }
  // \u6784\u9020\u51FD\u6570, \u5B9E\u4F8B\u5316\u7684\u65F6\u5019\u4F1A\u81EA\u52A8\u6267\u884C
  Person() {
    print(&quot;\u6211\u88AB\u5B9E\u4F8B\u5316\u4E86&quot;);
  }
}

void main() {

  // \u5B9E\u4F8B\u5316
  var p = new Person();
  p.name = &quot;panghu&quot;;
  p.age = 10;
  p.fun();
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br></div></div><h2 id="\u9759\u6001\u5C5E\u6027-\u65B9\u6CD5" tabindex="-1">\u9759\u6001\u5C5E\u6027/\u65B9\u6CD5</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>class Person {
  static String name; // \u9759\u6001\u53D8\u91CF
  int age;
  static fun() {
//    return age; // \u9759\u6001\u65B9\u6CD5,\u65E0\u6CD5\u8C03\u7528\u975E\u9759\u6001\u53D8\u91CF
//  print(this.name); // \u9759\u6001\u53D8\u91CF\u65E0\u6CD5\u901A\u8FC7this\u8C03\u7528
    print(name);
  }
}

void main() {

//  Person p = new Person();
//  p.fun(); // \u9759\u6001\u65B9\u6CD5\u65E0\u6CD5\u5B9E\u4F8B\u5316\u8C03\u7528
//  p.name;  // \u9759\u6001\u53D8\u91CF
//  p.age = 100;

  // \u9759\u6001\u65B9\u6CD5\u7684\u8C03\u7528
  Person.name = &quot;panghu&quot;;
  Person.fun();
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br></div></div><h2 id="\u8FDE\u7F00\u64CD\u4F5C" tabindex="-1">\u8FDE\u7F00\u64CD\u4F5C</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>class Person {
  String name;
  int age;
  String sex;
}

void main() {
  Person p = new Person();
  // \u8FDE\u7F00\u64CD\u4F5C
  p..name = &quot;panghu&quot;
   ..age = 10
   ..sex = &quot;\u7537&quot;;
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br></div></div><h2 id="\u7EE7\u627F" tabindex="-1">\u7EE7\u627F</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>class Person {
  String name;
  int age;
  fun() {
    print(&quot;\${name} --- \${age}&quot;);
  }
}
class panghu extends Person { // \u7EE7\u627F\u4E86Person\u7C7B

}
void main() {
  panghu p = new panghu();
  p.name = &quot;panghu&quot;;
  p.age = 10;
  p.fun();
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br></div></div><h2 id="\u591A\u6001" tabindex="-1">\u591A\u6001</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>abstract class Animal {   // \u62BD\u8C61\u7C7B, \u65E0\u6CD5\u88AB\u5B9E\u4F8B\u5316
  eat();  // \u62BD\u8C61\u65B9\u6CD5
  fun() {
    print(&quot;\u62BD\u8C61\u7C7B\u91CC\u9762\u7684\u666E\u901A\u65B9\u6CD5!&quot;);
  }
}

class Dog extends Animal {  // \u56E0\u4E3A\u7EE7\u627F\u62BD\u8C61\u7C7B,\u6240\u4EE5\u5FC5\u987B\u5B9E\u73B0\u91CC\u9762\u7684\u62BD\u8C61\u65B9\u6CD5
  @override
  eat() {
    print(&quot;\u5403\u8089&quot;);
  }

}

class Cat extends Animal {
  @override
  eat() {
    print(&quot;\u5403\u9C7C&quot;);
  }
}

void main() {
  Dog d = new Dog();
  d.eat();
  d.fun();

  Cat c = new Cat();
  c.eat();
  c.fun();

}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br></div></div><h2 id="\u63A5\u53E3" tabindex="-1">\u63A5\u53E3</h2><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>abstract class Animal {
  eat();
  fun() {
    print(&quot;\u62BD\u8C61\u7C7B\u91CC\u9762\u7684\u666E\u901A\u65B9\u6CD5!&quot;);
  }
}

class Dog implements Animal { // \u63A5\u53E3\u9700\u8981\u5B9E\u73B0\u91CC\u9762\u7684\u6240\u6709\u65B9\u6CD5
  @override
  eat() {
    print(&quot;\u5403\u8089&quot;);
  }

  @override
  fun() { // 

  }
}

void main() {
  Dog d = new Dog();
  d.eat();
  d.fun();
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br></div></div><h2 id="mixins" tabindex="-1">mixins</h2><p>\u505A\u4E3Amixins\u7684\u7C7B\u53EA\u80FD\u7EE7\u627FObject\u7C7B,\u4E0D\u80FD\u7EE7\u627F\u5176\u4ED6\u7C7B<br> \u505A\u4E3Amixins\u7684\u7C7B\u4E0D\u80FD\u6709\u6784\u9020\u51FD\u6570</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>class A {
  printA() {
    print(&quot;printA&quot;);
  }
}
class B {
  printB() {
    print(&quot;printB&quot;);
  }
}

//class c extends A, B{}  // \u65E0\u6CD5\u591A\u7EE7\u627F
class c with A, B {}  // mixins\u53EF\u4EE5\u7EE7\u627F\u591A\u4E2A\u7C7B
void main() {
  A a = new A();
  a.printA();
  B b = new B();
  b.printB();
}
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br></div></div>`,21);function r(l,p){return e}var i=n(a,[["render",r],["__file","function.html.vue"]]);export{i as default};
