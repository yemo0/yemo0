import{_ as n,d as s}from"./app.66431418.js";const a={},p=s(`<p>iota \u5E38\u91CF\u81EA\u52A8\u751F\u6210\u5668,\u6BCF\u96941\u884C\u81EA\u52A8\u52A01</p><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	<span class="token keyword">const</span> <span class="token punctuation">(</span>
		a <span class="token operator">=</span> <span class="token boolean">iota</span>
		b
		c
	<span class="token punctuation">)</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>a<span class="token punctuation">,</span> b<span class="token punctuation">,</span> c<span class="token punctuation">)</span>
	<span class="token comment">// \u7ED3\u679C\u4E3A1,2,3</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br></div></div><p>\u540C\u4E00\u884C,\u503C\u4E00\u6837</p><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code><span class="token keyword">func</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
	<span class="token keyword">const</span> <span class="token punctuation">(</span>
		a <span class="token operator">=</span> <span class="token boolean">iota</span>
		b1<span class="token punctuation">,</span> b2<span class="token punctuation">,</span> b3 <span class="token operator">=</span> <span class="token boolean">iota</span><span class="token punctuation">,</span> <span class="token boolean">iota</span><span class="token punctuation">,</span> <span class="token boolean">iota</span>
		c <span class="token operator">=</span> <span class="token boolean">iota</span>
	<span class="token punctuation">)</span>
	fmt<span class="token punctuation">.</span><span class="token function">Println</span><span class="token punctuation">(</span>a<span class="token punctuation">,</span> b1<span class="token punctuation">,</span> b2<span class="token punctuation">,</span> b3<span class="token punctuation">,</span> c<span class="token punctuation">)</span>
	<span class="token comment">// \u7ED3\u679C0,1,1,1,2</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br></div></div>`,4);function t(e,o){return p}var l=n(a,[["render",t],["__file","iota.html.vue"]]);export{l as default};
