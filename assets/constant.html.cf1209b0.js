import{_ as n,d as s}from"./app.66431418.js";const a={},e=s(`<p>\u5E38\u91CF\u662F\u4E00\u4E2A\u4E0D\u4F1A\u88AB\u4FEE\u6539\u7684\u91CF</p><p>\u5E38\u91CF\u4E2D\u7684\u6570\u636E\u7C7B\u578B\u53EA\u53EF\u4EE5\u662F\u5E03\u5C14\u578B\u3001\u6570\u5B57\u578B\uFF08\u6574\u6570\u578B\u3001\u6D6E\u70B9\u578B\u548C\u590D\u6570\uFF09\u548C\u5B57\u7B26\u4E32\u578B\u3002</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>\u5B9A\u4E49\u4E00\u4E2A\u5E38\u91CF
const a = 1
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><div class="language-go ext-go line-numbers-mode"><pre class="language-go"><code>\u5B9A\u4E49\u591A\u4E2A\u5E38\u91CF
<span class="token keyword">const</span> <span class="token punctuation">(</span>
	a <span class="token operator">=</span> <span class="token number">1</span>
	i <span class="token operator">=</span> <span class="token number">2</span>
<span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br></div></div>`,4);function t(r,p){return e}var l=n(a,[["render",t],["__file","constant.html.vue"]]);export{l as default};
